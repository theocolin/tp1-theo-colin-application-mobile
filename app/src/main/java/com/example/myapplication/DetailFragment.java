package com.example.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.myapplication.data.Country;

public class DetailFragment extends Fragment {

    ImageView Flag;
    TextView Name;
    TextView CapitalResult;
    TextView LangueResult;
    TextView SuperficieResult;
    TextView PopuResult;
    TextView MonnaieResult;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        int countryId = args.getCountryId();

        Name = view.findViewById(R.id.Name);
        Name.setText(Country.countries[countryId].getName());

        CapitalResult = view.findViewById(R.id.CapResult);
        CapitalResult.setText(Country.countries[countryId].getCapital());
        CapitalResult.setFocusable(false);

        LangueResult = view.findViewById(R.id.LangueResult);
        LangueResult.setText(Country.countries[countryId].getLanguage());
        LangueResult.setFocusable(false);

        SuperficieResult = view.findViewById(R.id.SuperficieResult);
        SuperficieResult.setText(Integer.toString(Country.countries[countryId].getArea()) + " km²");
        SuperficieResult.setFocusable(false);

        PopuResult = view.findViewById(R.id.PopuResult);
        PopuResult.setText(Integer.toString(Country.countries[countryId].getPopulation()));
        PopuResult.setFocusable(false);

        MonnaieResult = view.findViewById(R.id.MonnaieResult);
        MonnaieResult.setText(Country.countries[countryId].getCurrency());
        MonnaieResult.setFocusable(false);

        Flag = view.findViewById(R.id.Flag);
        String FlagName = Country.countries[countryId].getImgUri();
        Context c = this.getContext();
        Flag.setImageResource(c.getResources().getIdentifier(FlagName, null, c.getPackageName()));

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}